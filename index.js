const domDisplayLetter = document.getElementById('display-letter');
const domWordToDisplay = document.getElementById('word-to-display');
const domTranslatedLetter = document.getElementById('translated-letter');
const node = document.getElementById("input-word");

let alphabet = "abcdefghijklmnopqrstuvwxyz";

// Split alphabet string to letter array
alphabetArray = alphabet.split("");

// Create links object with letters as keys and links as values
let baseURl = "https://wpclipart.com/sign_language/American_ABCs/";
let links = {};
alphabetArray.forEach(element => {
    links[element] = baseURl + element + ".png";
});

// Event listener for Enter key on input
node.addEventListener("keyup", function(event) {
    if (event.key === "Enter") {
        // Do work
        domDisplayLetter.innerHTML = '';
        console.log(node.value);
        if(!node.value.match(/[a-z]/i)){
            // Check for alphabetic input
            alert('You entered invalid characters');
        } else {
            translate(node.value);
        }
    }
});

const translate = word => {
    domWordToDisplay.innerHTML = 'Word to translate: ' + word;
    word = word.toLowerCase().split("");
    // But what about spaces? :p

    word.forEach(char => {
        console.log(links[char]);
        if(char.match(/[a-z]/i)){
            console.log(char + ' is a letter');
            domDisplayLetter.innerHTML += '<img src="' + links[char] +'">' + char;
            // + '<figcaption>' + char + '</figcaption></li>';
        } else {
            alert('Could not translate character ' + char);
        }
    });
}